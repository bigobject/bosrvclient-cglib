INC =\
    -I./include \
    -I/usr/lib/x86_64-linux-gnu/glib-2.0/include \
    -I/usr/include/glib-2.0
CFLAGS = -Wall -fPIC
#LDFLAGS =\
    ../../thrift-no-boost-ptr/lib/c_glib/.libs/libthrift_c_glib.a
LDFLAGS =\
    /usr/local/lib/libthrift_c_glib.a

_BibObjectClient_c_glib_OBJ =\
	src/big_object_service.o \
   	src/big_object_service_types.o \
   	src/exception_types.o

RELEASE_BibObjectClient_c_glib_OBJ = $(patsubst src/%, .lib/%, $(_BibObjectClient_c_glib_OBJ))

OUT_DIR = .lib

LIB = libbosrvclient.a

.PHONY: all
all : directory $(LIB)

.PHONY: directory
directory :
	mkdir -p $(OUT_DIR)

$(LIB) : $(RELEASE_BibObjectClient_c_glib_OBJ)
	$(AR) cr .lib/$(LIB) .lib/*.o

$(RELEASE_BibObjectClient_c_glib_OBJ) : .lib/%.o : ./src/%.c
	$(CC) -c $(CFLAGS) -O3 $(INC) $< -o $@

.PHONY: clean
clean :
	rm -rf $(LIB) $(OUT_DIR)
